package notification_system;

import interfaces.INotifierFinder;
import interfaces.Notifier;
import utils.Configuration;
import utils.FileFinder;

public class NotifierFactory {
	
	private static Notifier notifierByConsole = null;
	private static Notifier notifierByEmail = null;
	private static Notifier notifierByTelegram = null;
	private static INotifierFinder notifierFinder;
	
	
	public static Notifier getNotifier(String notificationMode, INotifierFinder notifierFinder) {
		System.out.println("MODO: " + notificationMode);
		setNotifierFinder(notifierFinder);
		if(notificationMode.equals(Configuration.NOTIFY_MODE_EMAIL)) {
			return getNotifierByEmail();
		}else if(notificationMode.equals(Configuration.NOTIFY_MODE_TELEGRAM)) {
			return getNotifierByTelegram();
		}else {
			return getNotifierByConsole(); 	
		}
	}
	
	private static Notifier getNotifierByConsole() {
		Notifier notifier = null;
		String pathFinder = (String) Configuration.getProperties().get(Configuration.DIRECTORY_NAME_FIND_CONSOLE_NOTIFIER);
		notifier = getNotifierFinder().find(pathFinder, Configuration.NOTIFY_MODE_CONSOLE);
		return notifier;
	}
	
	private static Notifier getNotifierByTelegram() {
		if(notifierByTelegram != null) {
			return notifierByTelegram;
		}else {
			String pathFinder = (String) Configuration.getProperties().get(Configuration.DIRECTORY_NAME_FIND_TELEGRAM_NOTIFIER);
			notifierByTelegram = getNotifierFinder().find(pathFinder, Configuration.NOTIFY_MODE_TELEGRAM);
		}
		return notifierByTelegram;
	}

	private static Notifier getNotifierByEmail() {
		// TODO Auto-generated method stub
		if(notifierByEmail != null) {
			return notifierByEmail;
		}else {
			String pathFinder = (String) Configuration.getProperties().get(Configuration.DIRECTORY_NAME_FIND_MAIL_NOTIFIER);
			notifierByEmail = getNotifierFinder().find(pathFinder, Configuration.NOTIFY_MODE_EMAIL);
		}
		return notifierByEmail;
	}

	public static INotifierFinder getNotifierFinder() {
		return notifierFinder;
	}

	public static void setNotifierFinder(INotifierFinder notifierFinder) {
		NotifierFactory.notifierFinder = notifierFinder;
	}
}
