package notification_system;

import interfaces.INotification;

import java.util.Observable;

public class NotificationReceptor extends Observable {

    private INotification notification;

    public void notifyChanges(INotification notification) {
        this.setChanged();
        this.notifyObservers(notification);
    }
}
