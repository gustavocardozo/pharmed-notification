package notification_system;

import interfaces.INotification;
import interfaces.Notifier;

public class Notificator {
    private Notifier notifier;

    public Notificator(){
    }

    public void notify(INotification notification){
        notifier.notify(notification);
    }

	public void setNotifier(Notifier notifier) {
		this.notifier = notifier;
	}
}
