package notification_system;

import interfaces.INotification;
import interfaces.INotifierFinder;
import interfaces.Notifier;
import modelo.SuscriptionServer;

import java.util.Observable;
import java.util.Observer;

public class NotificationManager implements Observer {

    private Notificator notificator;
    private INotifierFinder notifierFinder;
    
    public NotificationManager(Notificator notificator, INotifierFinder notifierFinder) {
    	this.notificator = notificator;
    	this.notifierFinder = notifierFinder;
    }

    @Override
    public void update(Observable observable, Object o) {
        notifyByMode((INotification) o);
    }

    private void notifyByMode(INotification notification) {
    	Notifier notifier = NotifierFactory.getNotifier(notification.getMode(), notifierFinder);
    	notify(notification, notifier);
    }

    private void notify(INotification notification, Notifier notifier) {
    	System.out.print("NOIFICANDO: " + notifier);
    	if(notifier != null) {
        	notificator.setNotifier(notifier);
            notificator.notify(notification);
            SuscriptionServer.getInstance().addNotification(notification);
        }
    }
}
