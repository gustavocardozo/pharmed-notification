package interfaces;

public interface Notifier {
    public void notify(INotification notification);
}
