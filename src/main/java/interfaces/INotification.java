package interfaces;

import modelo.ChangeQuantity;

public interface INotification {
    public String getMessage();

    public String getMode();
    
    public String getClient();

    public String getRemedyName();

}
