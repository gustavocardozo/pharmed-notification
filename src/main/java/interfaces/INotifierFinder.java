package interfaces;

public interface INotifierFinder {
	public Notifier find(String pckgname, String notifyMode);
}
