package modelo;

import notification_system.NotificationReceptor;

import java.util.Observable;
import java.util.Observer;

public class CaptadorDeCambios extends Observable implements Observer {
    private NotificationReceptor receptor;

    public CaptadorDeCambios(NotificationReceptor receptor) {
        this.receptor = receptor;
    }

    @Override
    public void update(Observable observable, Object o) {
        Remedy remedy = (Remedy) o;
        setChanged();
        SuscriptionServer.getInstance(receptor).sendNotification(remedy.getName(), new ChangeQuantity(remedy, remedy.getTotal()));
    }


}
