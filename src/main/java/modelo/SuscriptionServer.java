package modelo;

import interfaces.INotification;
import notification_system.NotificationReceptor;

import java.util.*;

public class SuscriptionServer extends Observable {
    public static String defaultChannel = "stock";

    private Hashtable<String, List<RemedySubscriber>> subscriberLists;
    private List<INotification> notificationsCompleted;

    private static SuscriptionServer serverInstance;
    private NotificationReceptor receptor;
    private NotificationMaker maker = new NotificationMaker();

    public static SuscriptionServer getInstance(NotificationReceptor receptor) {
        if (serverInstance == null) {
            serverInstance = new SuscriptionServer();
            serverInstance.receptor = receptor;
        }
        return serverInstance;
    }

    public static SuscriptionServer getInstance() {
        if (serverInstance == null) {
            serverInstance = new SuscriptionServer();
        }
        return serverInstance;
    }

    private SuscriptionServer() {
        this.subscriberLists = new Hashtable<>();
        this.notificationsCompleted = new ArrayList<INotification>();
    }

    public void sendNotification(String remedyName, ChangeQuantity change) {
        List<RemedySubscriber> subs = subscriberLists.get(remedyName);
        if (subs != null) {
            for (RemedySubscriber s : subs) {
                if (s.isServiceActived()) {
                    s.receivedNotification(maker.createNotification(change, remedyName, s.getMode(), s.getClient()), receptor);
                }
            }
        }
    }

    public void registerSubscriber(String remedy, String client, String mode) {
        if (subscriberLists.get(remedy) == null) {
            subscriberLists.put(remedy, new ArrayList<>());
        }
        if (!isRegisterSubscriber(remedy, client, mode)) {
            subscriberLists.get(remedy).add(new RemedySubscriber(remedy, client, mode));
        }
    }

    public boolean isRegisterSubscriber(String remedy, String client, String mode) {
        if (subscriberLists.get(remedy) == null) {
            return false;
        }
        for (RemedySubscriber suscriptor : subscriberLists.get(remedy)) {
            if (suscriptor.getClient() == client && suscriptor.getMode() == mode) {
                return true;
            }
        }
        return false;
    }

    public void addNotification(INotification notification) {
        this.notificationsCompleted.add(notification);
    }

    public List<INotification> getNotificationsCompleted() {
        return this.notificationsCompleted;
    }

    public INotification getLastNotification() {
        return this.notificationsCompleted.size() > 0 ? this.notificationsCompleted.get(this.notificationsCompleted.size() - 1) : null;
    }

    public void removeNotifications() {
        this.notificationsCompleted = new ArrayList<INotification>();
    }


    public void changeServiceMode(boolean status, String remedyName, String client, String mode) {
        List<RemedySubscriber> suscriptores = subscriberLists.get(remedyName);
        if (suscriptores != null) {
            for (RemedySubscriber suscriptor : suscriptores) {
                if (suscriptor.getClient() == client && suscriptor.getMode() == mode) {
                    suscriptor.setServiceActived(status);
                    setChanged();
                    notifyObservers(suscriptor); // para que termine mostrándose en la vista
                }
            }
        }
    }
}
