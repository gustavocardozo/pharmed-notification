package modelo;

import interfaces.INotification;
import utils.Configuration;

public class NotificationMessage implements INotification {

    private String message = "";
    private String mode = Configuration.NOTIFY_MODE_CONSOLE;// Console is default
    private String client = "";
    private String remedyName = "";

    public NotificationMessage(String remedyName, String client, String message, String mode){
        this.message = message;
        this.mode = mode;
        this.remedyName = remedyName;
        this.client = client;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String getMode() {
        return mode;
    }

    @Override
    public String getClient() {
    	return client;
    }

    @Override
    public String getRemedyName() {
        return remedyName;
    }

 }
