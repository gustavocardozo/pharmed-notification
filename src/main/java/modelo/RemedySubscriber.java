package modelo;

import interfaces.INotification;
import notification_system.NotificationReceptor;

public class RemedySubscriber {
    private String medicament;
    private String client;
    private String mode;
    private boolean notified = false;
    private boolean serviceActived = true;

    public RemedySubscriber(String medicament, String client, String mode) {
        setMedicament(medicament);
        setClient(client);
        setMode(mode);
    }

    public void receivedNotification(INotification notification, NotificationReceptor receptor) {
        receptor.notifyChanges(notification);
        this.notified = true;
    };

    public String getMedicament() {
        return medicament;
    }

    public void setMedicament(String medicament) {
        this.medicament = medicament;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public boolean isNotified() {
        return notified;
    }

    public void setNotified(boolean notified) {
        this.notified = notified;
    }

    public boolean isServiceActived() {
        return serviceActived;
    }

    public void setServiceActived(boolean serviceActived) {
        this.serviceActived = serviceActived;
    }
}
