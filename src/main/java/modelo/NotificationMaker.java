package modelo;

public class NotificationMaker {

    public NotificationMaker() {
    }

    public NotificationMessage createNotification(ChangeQuantity change, String data, String mode, String client){
        String message = "Hubo un cambio de stock para el medicamento " + data + ". Ahora la cantidad es " + change.getNewQuantity();
        NotificationMessage notification = new NotificationMessage(change.getRemedy().getName(), client, message, mode);
        return notification;
    }
}
