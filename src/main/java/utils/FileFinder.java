package utils;

import interfaces.*;
import org.apache.http.impl.conn.SystemDefaultRoutePlanner;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author fdrod
 */
public class FileFinder implements INotifierFinder{
	
    @Override
	public Notifier find(String pckgname, String notifyMode) {
        File directory = new File(pckgname);

        if (directory.exists()) {
            String[] files = directory.list();
            for (int i = 0; i < files.length; i++) {
                if (files[i].endsWith(".class")) {
                    String fileName = files[i];
                    String classname = fileName.substring(0, files[i].length() - 6);
                    String nameClass = buildNameClase(pckgname, classname);
                    Object o = null;
                    try {
                        o = Class.forName(classname).getConstructor().newInstance();
                    } catch (InvocationTargetException | ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException e) {
                        e.printStackTrace();
                    }
                    if (checkInstance(o, notifyMode)) {
                        return (Notifier) o;
                    }
                }
            }
        }
        return null;
    }

    public static String buildNameClase(String pckgname, String classname) {
        String stringBase = "%s.%s";
        String packageName = "";
        for (int i = pckgname.length(); i > 0; i--) {
            char c = pckgname.charAt(i - 1);
            if (c == '\\' || c == '/') {
                break;
            }
            packageName = c + packageName;
        }
        return String.format(stringBase, packageName, classname);
    }

    public boolean checkInstance(Object o, String notifyMode) {
        if (notifyMode.equals(Configuration.NOTIFY_MODE_CONSOLE)) {
            return (o instanceof NotifierConsole);
        } else if (notifyMode.equals(Configuration.NOTIFY_MODE_EMAIL)) {
            return (o instanceof NotifierEmail);
        } else if (notifyMode.equals(Configuration.NOTIFY_MODE_TELEGRAM)) {
            return (o instanceof NotifierTelegram);
        }
        return false;
    }
}
