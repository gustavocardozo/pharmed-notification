package utils;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

public class Configuration {
    //Properties data
    private static Properties properties;
    public static final String PROPERTIE_PATH = "system_notification.properties";
    public static final String DIRECTORY_NAME_FIND_TELEGRAM_NOTIFIER ="directory_name_find_telegram_notifier";
    public static final String DIRECTORY_NAME_FIND_CONSOLE_NOTIFIER ="directory_name_find_console_notifier";
    public static final String DIRECTORY_NAME_FIND_MAIL_NOTIFIER ="directory_name_find_mail_notifier";
    public static final String TEST_ID_TELEGRAM ="test_id_telegram";

    //Global constants
    public static final String NOTIFY_MODE_CONSOLE ="notify_mode_console";
    public static final String NOTIFY_MODE_EMAIL ="notify_mode_email";
    public static final String NOTIFY_MODE_TELEGRAM ="notify_mode_telegram";

    public static Properties getProperties(){
        if(properties == null){
            properties = new Properties();
            properties.setProperty(DIRECTORY_NAME_FIND_TELEGRAM_NOTIFIER,"");
            properties.setProperty(DIRECTORY_NAME_FIND_CONSOLE_NOTIFIER,"");
            properties.setProperty(DIRECTORY_NAME_FIND_MAIL_NOTIFIER,"");
            properties.setProperty(TEST_ID_TELEGRAM,"");
            try {
                properties.store(new FileWriter(PROPERTIE_PATH),"");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return properties;
    }
}
