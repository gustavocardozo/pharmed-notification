package utils;

import interfaces.INotifierFinder;
import interfaces.Notifier;
import notificators.NotifierByConsole;
import notificators.NotifierByEmail;
import notificators.NotifierByTelegram;

public class NotifierFinderTest extends FileFinder {
	
	@Override
	public Notifier find(String pckgname, String notifyMode) {
		
		if(notifyMode.equals(Configuration.NOTIFY_MODE_CONSOLE)) {
			return new NotifierByConsole();
		}else if(notifyMode.equals(Configuration.NOTIFY_MODE_TELEGRAM)){
			return new NotifierByTelegram();
		}else if(notifyMode.equals(Configuration.NOTIFY_MODE_EMAIL)){
			return new NotifierByEmail();
		}
		
		return null;
	}
}
