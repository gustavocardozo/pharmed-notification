package utils;

import org.junit.Test;

import interfaces.INotifierFinder;
import notificators.NotifierByConsole;
import notificators.NotifierByEmail;
import notificators.NotifierByTelegram;

import static org.junit.Assert.*;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Before;

public class FileFinderTest {

	private FileFinder notifierFinder;
	private NotifierByConsole notifierConsole;
	private NotifierByTelegram notifierTelegram;
	private NotifierByEmail notifierEmail;
	
	@Before
	public void beforeTest() {
		notifierFinder = new FileFinder();
		notifierConsole = new NotifierByConsole();
		notifierEmail = new NotifierByEmail();
		notifierTelegram = new NotifierByTelegram();
		
	}
	
    @Test
    public void find() throws ClassNotFoundException{
        notifierFinder.find("", Configuration.NOTIFY_MODE_TELEGRAM);
    }
    
    @Test
    public void checkInstanceTest() {
    	assertTrue(notifierFinder.checkInstance(notifierConsole, Configuration.NOTIFY_MODE_CONSOLE));
    	assertTrue(notifierFinder.checkInstance(notifierEmail, Configuration.NOTIFY_MODE_EMAIL));
    	assertFalse(notifierFinder.checkInstance(notifierConsole, Configuration.NOTIFY_MODE_TELEGRAM));
    	assertFalse(notifierFinder.checkInstance(notifierConsole, ""));
    }
    
    @Test
    public void buildNameClaseTest() {
    	String correct = "hola.mundo";
    	String incorrect = "chau.mundo";
    	String part1 = "hola";
    	String part2 = "mundo";
    	String part3 = "/hola";
    	assertEquals(correct, notifierFinder.buildNameClase(part1, part2));
    	assertNotEquals(incorrect, notifierFinder.buildNameClase(part1, part2));
    	assertEquals(correct, notifierFinder.buildNameClase(part3, part2));
    }
}