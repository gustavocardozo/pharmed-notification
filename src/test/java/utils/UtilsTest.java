package utils;

import org.junit.Test;

import interfaces.INotifierFinder;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;

public class UtilsTest {
    
    @Test
    public void testValidateString() throws Exception {
        assertTrue(Utilities.validateString("okey"));
        assertFalse(Utilities.validateString(""));
        assertFalse(Utilities.validateString(null));
    }
}
