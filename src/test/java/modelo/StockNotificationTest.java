package modelo;

import interfaces.INotification;
import interfaces.INotifierFinder;
import interfaces.Notifier;
import interfaces.NotifierEmail;
import interfaces.NotifierTelegram;
import notification_system.NotificationManager;
import notification_system.NotificationReceptor;
import notification_system.Notificator;
import notification_system.NotifierFactory;
import org.junit.Before;
import org.junit.Test;
import utils.Configuration;
import utils.NotifierFinderTest;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import static org.junit.Assert.*;

public class StockNotificationTest {
	private Stock stock;
	private NotificationReceptor notificationReceptor;
	private NotificationManager notificationManager;
	private INotifierFinder notifierFinder;

	@Before
	public void initStockAndSuscription() {
		stock = new Stock();
		notifierFinder =  new NotifierFinderTest();
		NotifierFactory notifierFactory = new NotifierFactory();
		Configuration configuration = new Configuration();
		// seteo el nombre del package en el core
		// Configuration.setPackageNameFind("notificators");
		Properties properties = Configuration.getProperties();
		try {
			properties.load(new FileReader(Configuration.PROPERTIE_PATH));
			properties.setProperty(Configuration.DIRECTORY_NAME_FIND_TELEGRAM_NOTIFIER, "notificators");
			properties.setProperty(Configuration.DIRECTORY_NAME_FIND_CONSOLE_NOTIFIER, "notificators");
			properties.setProperty(Configuration.DIRECTORY_NAME_FIND_MAIL_NOTIFIER, "notificators");
			properties.store(new FileWriter(Configuration.PROPERTIE_PATH), "EDITADO");
		} catch (IOException e) {
			e.printStackTrace();
		}
		// Realiza las instancias de los distintos componentes del sistema de
		// notificacion

		notificationReceptor = new NotificationReceptor();
		// captadorDeCambios.addObserver(notificationReceptor);
		notificationManager = new NotificationManager(new Notificator(),notifierFinder);
		notificationReceptor.addObserver(notificationManager);

		CaptadorDeCambios captadorDeCambios = new CaptadorDeCambios(notificationReceptor);
		stock.addObserver(captadorDeCambios);
		SuscriptionServer.getInstance(notificationReceptor).removeNotifications();
	}

	@Test
	public void testMedicamentNotificationOk() throws Exception {
		String remedyName = "IBUPROFENO";
		SuscriptionServer.getInstance().registerSubscriber(remedyName, "Juan", Configuration.NOTIFY_MODE_CONSOLE);
		stock.addRemedy(remedyName, 20);
		INotification lastNotification = SuscriptionServer.getInstance().getLastNotification();
		assertTrue(lastNotification.getRemedyName() == remedyName);
	}

	@Test(expected = Exception.class)
	public void testNonexistenMedicament() throws Exception {
		stock.addRemedy("AMOXICILINA", 5);
		assertTrue(stock.getStock().size() == 1);
		int quantity = stock.checkQuantity("NOEXISTEENSTOCK");
	}

	@Test
	public void testWithoutChanges() throws Exception {
		String remedyName = "CEFALEXINA";
		stock.createRemedy(remedyName, 20);
		stock.extractRemedy(remedyName, 5);
		assertNull(SuscriptionServer.getInstance().getLastNotification());
	}

	@Test
	public void testNegativeQuantityNotification() throws Exception {
		String remedyName = "BAYASPIRINA";
		stock.createRemedy(remedyName, -100);
		assertNull(SuscriptionServer.getInstance().getLastNotification());
	}

	@Test
	public void testDiferentMedicamentNotification() throws Exception {
		SuscriptionServer.getInstance().registerSubscriber("KETROLAC", "Mario", Configuration.NOTIFY_MODE_CONSOLE);
		String remedyName = "PENICILINA";
		stock.createRemedy(remedyName, 100);
		INotification lastNotification = SuscriptionServer.getInstance().getLastNotification();
		String lastRemedy = (lastNotification != null) ? lastNotification.getRemedyName() : "";
		assertTrue(lastRemedy != remedyName);
	}

	@Test
	public void testDataNotification() throws Exception {
		NotificationMessage notification = new NotificationMessage("Remedio prueba", "Mario", "mensaje",
				Configuration.NOTIFY_MODE_CONSOLE);
		String notificationMessage = notification.getMessage();
		String cliente = notification.getClient();
		assertTrue(notificationMessage != null);
		assertTrue(!notificationMessage.isEmpty());
		assertTrue(cliente.equals("Mario"));
	}

	@Test
	public void testDataRemedySuscriber() throws Exception {
		RemedySubscriber remedySubscriber = new RemedySubscriber("Tafirol", "Mario", Configuration.NOTIFY_MODE_CONSOLE);
		String newRemedy = "Ibuprofeno";
		String newClient = "Tania";
		String newMode = Configuration.NOTIFY_MODE_EMAIL;
		remedySubscriber.setMedicament(newRemedy);
		remedySubscriber.setClient(newClient);
		remedySubscriber.setMode(newMode);
		remedySubscriber.setNotified(true);
		assertTrue(newRemedy.equals(remedySubscriber.getMedicament()));
		assertTrue(newClient.equals(remedySubscriber.getClient()));
		assertTrue(newMode.equals(remedySubscriber.getMode()));
		assertTrue(remedySubscriber.isNotified());
	}

	@Test
	public void testMultiplesSuscriptions() throws Exception {
		SuscriptionServer.getInstance().removeNotifications();
		String remedyName = "BUSCAPINA";
		SuscriptionServer.getInstance().registerSubscriber(remedyName, "Mario", Configuration.NOTIFY_MODE_CONSOLE);
		SuscriptionServer.getInstance().registerSubscriber(remedyName, "Carlos", Configuration.NOTIFY_MODE_CONSOLE);
		stock.addRemedy(remedyName, 100);
		List<INotification> completed = SuscriptionServer.getInstance().getNotificationsCompleted();
		assertEquals(2, completed.size());
		assertTrue(completed.get(0).getClient() == "Mario" || completed.get(0).getClient() == "Carlos");
		assertTrue(completed.get(1).getClient() == "Mario" || completed.get(1).getClient() == "Carlos");
	}

	@Test
	public void testChangeQuantity() throws Exception {
		ChangeQuantity changeQuantity = new ChangeQuantity(new Remedy("Paracetamol", 20), 20);
		Remedy remedy = new Remedy("Bayaspirina", 20);
		changeQuantity.setRemedy(remedy);
		changeQuantity.setNewQuantity(15);
		assertEquals(remedy, changeQuantity.getRemedy());
		assertTrue(15 == changeQuantity.getNewQuantity());
	}

	@Test
	public void testNotifierEmailInstance() {
		NotifierEmail notifier = (NotifierEmail) NotifierFactory.getNotifier(Configuration.NOTIFY_MODE_EMAIL, notifierFinder);
		assertNotNull(notifier);
		assertTrue(notifier instanceof Notifier);
	}

	@Test
	public void testSendNotificationByEmail() {
		String remedyName = "Sertal";
		SuscriptionServer.getInstance().registerSubscriber(remedyName, "Mario", Configuration.NOTIFY_MODE_EMAIL);
		SuscriptionServer.getInstance().sendNotification(remedyName, new ChangeQuantity(new Remedy(remedyName, 0), 20));

		INotification lastNotification = SuscriptionServer.getInstance().getLastNotification();
		String lastRemedy = (lastNotification != null) ? lastNotification.getRemedyName() : "";
		assertTrue(lastRemedy == remedyName);
	}

	// Test for Telegram

	@Test
	public void testNotifierTelegramInstance() {
		NotifierTelegram notifier = (NotifierTelegram) NotifierFactory.getNotifier(Configuration.NOTIFY_MODE_TELEGRAM, notifierFinder);
		assertNotNull(notifier);
		assertTrue(notifier instanceof Notifier);
	}

	@Test
	public void testSendNotificationByTelegram() throws Exception {
		SuscriptionServer.getInstance().removeNotifications();
		String remedyName = "IBUPROFENO";
		SuscriptionServer.getInstance().registerSubscriber(remedyName, "Mario", Configuration.NOTIFY_MODE_TELEGRAM);
		SuscriptionServer.getInstance().sendNotification(remedyName, new ChangeQuantity(new Remedy(remedyName, 0), 20));
		INotification lastNotification = SuscriptionServer.getInstance().getLastNotification();
		String lastRemedy = (lastNotification != null) ? lastNotification.getRemedyName() : "";
		assertTrue(lastRemedy == remedyName);
		assertTrue(lastNotification.getClient() == "Mario");
	}

	@Test
	public void testTelegramMode() throws Exception {
		String remedyName = "PASURTA";
		SuscriptionServer.getInstance().registerSubscriber(remedyName, "Jorge", Configuration.NOTIFY_MODE_TELEGRAM);
		SuscriptionServer.getInstance().sendNotification(remedyName,
				new ChangeQuantity(new Remedy(remedyName, 0), 100));
		INotification lastNotification = SuscriptionServer.getInstance().getLastNotification();
		assertTrue(lastNotification.getMode() == Configuration.NOTIFY_MODE_TELEGRAM);
	}

	@Test
	public void testErrorMode() throws Exception {
		String remedyName = "ASPIRINA";
		SuscriptionServer.getInstance().registerSubscriber(remedyName, "Esteban", "MODOERRONEO");
		SuscriptionServer.getInstance().removeNotifications();
		SuscriptionServer.getInstance().sendNotification(remedyName, new ChangeQuantity(new Remedy(remedyName, 0), 60));
		assertTrue(
				SuscriptionServer.getInstance().getLastNotification().getMode() != Configuration.NOTIFY_MODE_TELEGRAM);
	}
	
    @Test
    public void testChangeStatusMode() throws Exception {
        String remedyName = "ASPIRINA";
        SuscriptionServer.getInstance().registerSubscriber(remedyName,"Esteban", Configuration.NOTIFY_MODE_CONSOLE);
        SuscriptionServer.getInstance().changeServiceMode(false,remedyName, "Esteban",Configuration.NOTIFY_MODE_CONSOLE);
    }
}
