package notificators;

import interfaces.INotification;
import interfaces.NotifierTelegram;

public class NotifierByTelegram implements NotifierTelegram {

    public NotifierByTelegram() {
        System.out.println("NOMBRE DE CLASE: " + getClass().getName());
    }

    @Override
    public void notify(INotification notification) {
        System.out.println("NOTIFICACIóN: " + notification.getMessage());
    }
}
